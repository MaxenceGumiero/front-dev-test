import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { useSelector } from 'react-redux';

import { RootState } from '../store';

// Service
import { fetchMovie, fetchMovieSearchResult } from '../../services/OMDbService';

// Models
import { Movie } from '../../models/Movie';
import { MovieSearchResult } from '../../models/MovieSearchResult';
import { MovieState } from '../../models/MovieState';

const initialState: MovieState = {
  movieSearchResult: { Search: [] },
  movie: {
    Actors: '',
    Awards: '',
    BoxOffice: '',
    Country: '',
    DVD: '',
    Director: '',
    Genre: '',
    Language: '',
    Metascore: '',
    Plot: '',
    Poster: '',
    Production: '',
    Rated: '',
    Ratings: [],
    Released: '',
    Response: '',
    Runtime: '',
    Title: '',
    Type: '',
    Website: '',
    Writer: '',
    Year: '',
    imdbID: '',
    imdbRating: '',
    imdbVotes: ''
  },
  search: 'Batman',
  loading: true
};

// Movie Slice

const moviesSlice = createSlice({
  name: 'movies',
  initialState,
  reducers: {
    setMovieSearchResult: (state, { payload }: PayloadAction<MovieSearchResult>) => {
      state.movieSearchResult = payload;
    },
    setMovie: (state, { payload }: PayloadAction<Movie>) => {
      state.movie = payload;
    },
    setSearch: (state, { payload }: PayloadAction<string>) => {
      state.search = payload;
    },
    setLoading: (state, { payload }: PayloadAction<boolean>) => {
      state.loading = payload;
    }
  }
});

// Movie Slice Actions

export const getMovieSearchResult = (search: string) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    dispatch(setMovieSearchResult(await fetchMovieSearchResult(search)));
    dispatch(setLoading(false));
  } catch (err) {
    console.log(err);
    dispatch(setLoading(false));
  }
};

export const selectMovieSearchResult = (state: RootState) => state.movies;

export const getMovie = (id: string) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    dispatch(setMovie(await fetchMovie(id)));
    dispatch(setLoading(false));
  } catch (err) {
    console.log(err);
    dispatch(setLoading(false));
  }
};

export const selectMovie = (state: RootState) => state.movies;

export const getSearch = (search: string) => (dispatch) => {
  dispatch(setLoading(true));
  try {
    dispatch(setSearch(search));
    dispatch(setLoading(false));
  } catch (err) {
    dispatch(setLoading(false));
  }
};

export const selectSearch = (state: RootState) => state.movies;

export const { setMovieSearchResult, setMovie, setSearch, setLoading } = moviesSlice.actions;

export default moviesSlice.reducer;
