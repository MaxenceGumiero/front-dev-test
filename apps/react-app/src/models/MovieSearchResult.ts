import { MoviePreview } from './MoviePreview';

export interface MovieSearchResult {
  Search: MoviePreview[];
}
