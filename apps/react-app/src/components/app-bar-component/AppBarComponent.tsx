import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Container from '@material-ui/core/Container';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const AppBarComponent = () => {
  return (
    <AppBar position="static">
      <Toolbar>
        <Container maxWidth='lg'>
          <Typography variant="h6">
            Front Dev Test
          </Typography>
        </Container>
      </Toolbar>
    </AppBar>
  );
};

export default AppBarComponent;
