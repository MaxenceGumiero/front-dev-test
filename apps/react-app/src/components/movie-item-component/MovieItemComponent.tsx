import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { MoviePreview } from '../../models/MoviePreview';
import Button from '@material-ui/core/Button';
import TableCell from '@material-ui/core/TableCell';
import Avatar from '@material-ui/core/Avatar';
import TableRow from '@material-ui/core/TableRow';

const useStyles = makeStyles({
  root: {
    '&:nth-of-type(odd)': {
      background: '#fafafa',
    },
  }
});

const MovieItemComponent: React.FC<MoviePreview> = (props) => {
  const classes = useStyles();

  return (
    <TableRow className={ classes.root }>
      <TableCell component='th' scope='row'>
        <Avatar alt={ `Poster of '${ props.Title }' props` } src={ props.Poster } />
      </TableCell>
      <TableCell component='th' scope='row'>
        { props.Title }
      </TableCell>
      <TableCell>{ props.Year }</TableCell>
      <TableCell>
        <Button variant='contained' color='primary' onClick={() => { window.open(`http://localhost:4200/${ props.imdbID }`, '_blank', 'width=1280, height=720') }}>Go to details</Button>
      </TableCell>
    </TableRow>
  );
};

export default MovieItemComponent;
