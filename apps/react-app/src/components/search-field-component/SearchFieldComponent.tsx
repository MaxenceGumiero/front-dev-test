import React from 'react';
import { useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { getSearch } from '../../redux/slices/moviesSlice';

const useStyles = makeStyles({
  textField: {
    width: '100%',
    marginBottom: 18
  }
});

const SearchFieldComponent: React.FC = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  return (
    <form noValidate autoComplete="off">
      <TextField
        id="outlined-basic"
        className={ classes.textField }
        label="Search"
        variant="outlined"
        onChange={ e => dispatch(getSearch(e.target.value)) }
      />
    </form>
  );
};

export default SearchFieldComponent;
