import { configureStore } from '@reduxjs/toolkit';
import moviesSlice from './slices/moviesSlice';

const store = configureStore({
  reducer: {
    movies: moviesSlice
  }
});

export type RootState = ReturnType<typeof store.getState>

export default store;
