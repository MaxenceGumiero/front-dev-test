import { Movie } from '../models/Movie';
import { MovieSearchResult } from '../models/MovieSearchResult';

export const apiURL = `https://www.omdbapi.com/`;
export const apiKey = `df5d3f05`;

export const fetchMovieSearchResult = async (search: string) => {
  try {
    const res = await fetch(`${ apiURL }?type=movie&s=${ search }&apikey=${ apiKey }`);
    const json = await res.json();
    return json as MovieSearchResult;
  } catch (err) {
    console.log(err);
  }
};

export const fetchMovie = async (id: string) => {
  try {
    const res = await fetch(`${ apiURL }?i=${ id }&plot=full&apikey=${ apiKey }`);
    const json = await res.json();
    return json as Movie;
  } catch (err) {
    console.log(err);
  }
};

