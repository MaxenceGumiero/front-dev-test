import { Movie } from './Movie';
import { MovieSearchResult } from './MovieSearchResult';

export interface MovieState {
  movieSearchResult: MovieSearchResult;
  movie: Movie;
  search: string;
  loading: boolean;
}
