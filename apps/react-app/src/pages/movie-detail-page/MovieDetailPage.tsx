import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { getMovie, selectMovie } from '../../redux/slices/moviesSlice';

// Models
import { Movie } from '../../models/Movie';

// Components
import CircularProgressComponent from '../../components/circular-progress-component/CircularProgressComponent';

interface Props {
  match: { params: { id: string; }},
  movie: Movie;
}

const useStyles = makeStyles({
  root: {
    padding: 18
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  cardImage: {
    display: 'flex',
    justifyContent: 'center',
    height: 220,
    width: 160,
    marginBottom: 24
  }
});

const MovieDetailPage: React.FC<Props> = (props) => {
  const classes = useStyles();
  const state = useSelector(selectMovie);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getMovie(props.match.params.id));
  }, [dispatch, props.match.params.id]);

  console.log(state);

  return (
    <>
      { !state.loading && !state.movie.Error ? (
        <>
          <AppBar position='static'>
            <Toolbar>
              <Container maxWidth='lg'>
                <Typography variant='h6'>
                  { state.movie.Title }
                </Typography>
              </Container>
            </Toolbar>
          </AppBar>
          <div className={ classes.root }>
            <Container maxWidth='lg'>
              <Card className={ classes.root } variant="outlined">
                <CardContent>
                  <Card className={ classes.cardImage }
                    style={{
                      background: `url(${ state.movie.Poster })`,
                      backgroundPosition: 'center',
                      backgroundSize: 'cover'
                    }} >
                  </Card>
                  <Typography className={ classes.pos } color="textSecondary">
                    <strong>Release year -</strong> { state.movie.Year }
                  </Typography>
                  <Typography className={ classes.pos } color="textSecondary">
                    <strong>Actors -</strong> { state.movie.Actors }
                  </Typography>
                  <Typography className={ classes.pos } variant="body2" component="p">
                    <strong>Plot -</strong> { state.movie.Plot }
                  </Typography>
                  <Button variant='contained' color="primary" onClick={() => { window.close() }}>Close</Button>
                </CardContent>
              </Card>
            </Container>
          </div>
        </>
      ) : state.movie.Error ? (
        <>
          <AppBar position='static'>
            <Toolbar>
              <Container maxWidth='lg'>
                <Typography variant='h6'>
                  <strong>Error -</strong> { state.movie.Error }
                </Typography>
              </Container>
            </Toolbar>
          </AppBar>
          <div className={ classes.root }>
            <Container maxWidth='lg'>
              <Button variant='contained' color="primary" onClick={() => { window.close() }}>Close</Button>
            </Container>
          </div>
        </>
      ) : (
        <CircularProgressComponent />
      ) }
    </>
  );
};

export default MovieDetailPage;
