import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { getMovieSearchResult, selectMovieSearchResult, selectSearch } from '../../redux/slices/moviesSlice';
import { MoviePreview } from '../../models/MoviePreview';

// Components
import MovieItemComponent from '../../components/movie-item-component/MovieItemComponent';
import CircularProgressComponent from '../../components/circular-progress-component/CircularProgressComponent';
import AppBarComponent from '../../components/app-bar-component/AppBarComponent';
import SearchFieldComponent from '../../components/search-field-component/SearchFieldComponent';

const useStyles = makeStyles({
  root: {
    padding: 18
  }
});

const MovieListPage: React.FC = () => {
  const classes = useStyles();
  const state = useSelector(selectMovieSearchResult, selectSearch);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getMovieSearchResult(state.search));
  }, [dispatch, state.search]);

  return (
    <>
      <AppBarComponent />
      <div className={ classes.root }>
        <Container maxWidth='lg'>
          <SearchFieldComponent />
          { !state.loading && !state.movie.Error && state.movieSearchResult.Search ? (
            <TableContainer component={ Paper }>
              <Table aria-label='simple table'>
                <TableHead>
                  <TableRow>
                    <TableCell><strong>Poster</strong></TableCell>
                    <TableCell><strong>Title</strong></TableCell>
                    <TableCell><strong>Year</strong></TableCell>
                    <TableCell><strong>Details</strong></TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  { state.movieSearchResult.Search.map((movie: MoviePreview) => (
                    <MovieItemComponent key={ movie.imdbID } { ...movie } />
                  )) }
                </TableBody>
              </Table>
            </TableContainer>
          ) : !state.loading && state.movieSearchResult.Error ? (
            <p><strong>Error -</strong> { state.movieSearchResult.Error }</p>
          ) : (
            <CircularProgressComponent />
          ) }
        </Container>
      </div>
    </>
  );
};

export default MovieListPage;
