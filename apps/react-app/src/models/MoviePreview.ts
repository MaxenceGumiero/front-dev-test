export interface MoviePreview {
  Title: string;
  Year: string;
  imdbID: string;
  Type: string;
  Poster: string;
}
