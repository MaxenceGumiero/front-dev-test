import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import MovieListPage from './pages/movie-list-page/MovieListPage';
import MovieDetailPage from './pages/movie-detail-page/MovieDetailPage';

const App: React.FC = () => {
  return (
    <Router>
      <Switch>
        <Route exact path='/' component={ MovieListPage } />
        <Route exact path='/:id' component={ MovieDetailPage } />
      </Switch>
    </Router>
  );
};

export default App;
